function play_video (name) {
    var video = $('<video />', {
        id: 'video',
        class:'video-player',
        src: 'assets/video/'+name+'.mp4',
        type: 'video/mp4',
        autoplay : true,
        controls: true
    });
    $('#video-section').css('height','unset');
    $('#video-section').html(video) ;
}

function nextVideo(elem,index) {
    $(elem).parent().parent().parent().removeClass('slide-left').addClass('slide-right');
    $("#video-section").children().toggleClass('active');
    $(".video-controls .right").toggleClass('d-none');
    $(".video-controls .left").removeClass('d-none');
}

function prevVideo(elem,index) {
    $(".video-controls .left").toggleClass('d-none');
    $(".video-controls .right").removeClass('d-none');
    $(elem).parent().parent().parent().removeClass('slide-right').addClass('slide-left');
    $("#video-section").children().toggleClass('active');
}

function nextDataset (elem) {
    const Dataset = [
        {
            'id'         : 1,
            'percentage' : '70.1%',
            'infos'      : '',
            'desc'       : 'من التوانسة يعتبروا أنو المصالحة الوطنية لازم تكون بتحقيق كل الشروط اللازمة لمسار العدالة الإنتقالية بداية بكشف الحقيقة، مرورا بمحاسبة مرتكبي الجرائم، وردّ الإعتبار للضحايا ووضع الضمانات اللازمة باش ما يعاودش يصير إلي صار قبل'
        },
        {
            'id'         : 2,
            'percentage' : '66.5%',
            'infos'      : '',
            'desc'       : 'من التوانسة يشوفوا أنو أهداف العدالة الانتقالية إلّي هوما كشف الحقيقة عن مختلف الانتهاكات مساءلة ومحاسبة المسؤولين عنها وجبر الضرر ورد الاعتبار للضحايا لتحقيق المصالحة الوطنية لازم كونوا موجودين في برامج السياسيين الي باش يكونوا في الحكم بعد انتخابات '
        },
    ]

    const index = ( $(elem).parent().data('dataset') == 1 ) ? 2 : 1;
    console.log($(elem).parent().data('dataset'));
    console.log(index);
    const data = Dataset[index - 1];

    if (data) {
        $("#sondage-percentage").text(data.percentage);
        $("#sondage-info").text(data.infos);
        $("#sondage-desc").text(data.desc);
        $(elem).parent().data('dataset',index);
    }


}

function toggleMenu() {
    $('#mobile-menu').toggleClass('open');
}

/** Jquery Scroll Animation **/
$(function () {
    $('a[href*=\\#]:not([href=\\#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('#home-menu').removeClass('open');
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});